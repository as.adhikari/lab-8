package day2.prob1;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyClass {
    private String title;

    MyClass(String title){
        this.title = title;
    }

    // we can use Predicate to test equals
    // I think we can also use Function interface
    public void myMethod(MyClass cl){
        Function<MyClass,Boolean> result = this::equals;
        Predicate<MyClass> checkObject = this::equals;

        System.out.println(result.apply(cl));
        System.out.println(checkObject.test(cl));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MyClass)) return false;
        MyClass myCls = (MyClass) obj;

        return getTitle().equals(myCls.getTitle());
    }

    public static void main(String[] args){
        MyClass mc1 = new MyClass("MyTitle");
        MyClass mc2 = new MyClass("MySecondTitle");
        MyClass mc3 = new MyClass("MySecondTitle");

        mc3.myMethod(mc2);
    }
}
