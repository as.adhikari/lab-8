package day2.prob4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args){
        String[] names = {"Alexis", "Tim", "Kyleen", "KRISTY"};

//        Arrays.sort(names, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.compareTo(o2);
//            }
//        });


        Arrays.sort(names,String::compareTo);


        for(String name:names){
            System.out.println(name);
        }
    }
}
