package day2.prob2;

import java.util.function.Supplier;

public class PartC {

    public static void main(String[] args) {
        Supplier<Double> randomNum = new Supplier<Double>() {
            @Override
            public Double get() {
                return Math.random();
            }
        };

        System.out.println(randomNum.get());

        PartC pc = new PartC();
        System.out.println(pc.getRandomNum());
    }

    public double getRandomNum(){
        RandNum randNum = new RandNum();

        return randNum.get();
    }

    class RandNum implements Supplier<Double>{

        @Override
        public Double get() {
            return Math.random();
        }
    }
}
