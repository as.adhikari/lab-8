package day2.prob2;

import java.util.function.Supplier;

public class PartB {
    public static void main(String[] args){

        Supplier<Double> randNum = () -> Math.random();

        System.out.println(randNum.get());
    }
}
