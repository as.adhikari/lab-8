package day2.prob2;

import java.util.function.Supplier;

public class PartA {
    public static void main(String[] args){

        Supplier<Double> randNum = Math::random;

        System.out.println(randNum.get());
    }
}
