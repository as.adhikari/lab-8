package day2.prob3;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args){
        List<String> fruits = Arrays.asList("Apple", "Banana","Orange","Cherries","blums");

        // Print the given list using forEach with Lambdas
        System.out.println("Printing using Lambdas");
        fruits.stream()
                .forEach((fruit) -> System.out.println(fruit));

        // Print the given list using method reference
        System.out.println("\n\nPrinting using Method Reference");
        fruits.stream()
                .forEach(System.out::println);
    }
}
