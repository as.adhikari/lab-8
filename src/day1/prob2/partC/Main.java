package day1.prob2.partC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    static enum SortMethod {TITLE, PRICE, MODEL};

    public static void main(String[] args) {
        List<Product> productList = new ArrayList<Product>();

        productList.add(new Product("IPhone", 999.0, 2018));
        productList.add(new Product("OnePlus 5T", 580.0, 2017));
        productList.add(new Product("Galaxy S9", 1100.0, 2018));
        productList.add(new Product("Google Pixel 2", 1150.0, 2017));
        productList.add(new Product("Xiaomi Mi Mix", 890.0, 2016));

        System.out.println("======== Sorting By Title ==========");
        Main main = new Main();
        main.sort(productList,SortMethod.TITLE);
        for (Product p : productList) {
            System.out.println(p);
        }

        System.out.println("\n======== Sorting By Price ==========");
        main.sort(productList,SortMethod.PRICE);
        for (Product p : productList) {
            System.out.println(p);
        }

        System.out.println("\n======== Sorting By Model ==========");
        main.sort(productList,SortMethod.MODEL);
        for (Product p : productList) {
            System.out.println(p);
        }
    }

    public  void sort(List<Product> products, final SortMethod sortBy) {
        class ProductComparator implements Comparator<Product> {
            @Override
            public int compare(Product o1, Product o2) {

                if (SortMethod.TITLE == sortBy) {
                    return o1.getTitle().compareTo(o2.getTitle());
                } else if (SortMethod.PRICE == sortBy) {
                    if (o1.getPrice() > o2.getPrice()) {
                        return 1;
                    } else if (o1.getPrice() < o2.getPrice()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else if (SortMethod.MODEL == sortBy) {
                    if (o1.getModel() > o2.getModel()) {
                        return 1;
                    } else if (o1.getModel() < o2.getModel()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            }
        }

        Collections.sort(products,new ProductComparator());
    }
}
