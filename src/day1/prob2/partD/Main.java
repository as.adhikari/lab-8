package day1.prob2.partD;

import day1.prob2.partC.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    static enum SortMethod {TITLE, PRICE, MODEL}

    ;

    public static void main(String[] args) {
        List<day1.prob2.partC.Product> productList = new ArrayList<day1.prob2.partC.Product>();

        productList.add(new day1.prob2.partC.Product("IPhone", 999.0, 2018));
        productList.add(new day1.prob2.partC.Product("OnePlus 5T", 580.0, 2017));
        productList.add(new day1.prob2.partC.Product("Galaxy S9", 1100.0, 2018));
        productList.add(new day1.prob2.partC.Product("Google Pixel 2", 1150.0, 2017));
        productList.add(new day1.prob2.partC.Product("Xiaomi Mi Mix", 890.0, 2016));

        System.out.println("======== Sorting By Title ==========");
        Main main = new Main();
        main.sort(productList, SortMethod.TITLE);
        for (Product p : productList) {
            System.out.println(p);
        }

        System.out.println("\n======== Sorting By Price ==========");
        main.sort(productList, SortMethod.PRICE);
        for (Product p : productList) {
            System.out.println(p);
        }

        System.out.println("\n======== Sorting By Model ==========");
        main.sort(productList, SortMethod.MODEL);
        for (Product p : productList) {
            System.out.println(p);
        }
    }

    public void sort(List<Product> products, final SortMethod method) {

        Collections.sort(products, (p1, p2) -> {
            if (SortMethod.MODEL == method) {
                if (p1.getModel() > p2.getModel()) {
                    return 1;
                } else if (p1.getModel() < p2.getModel()) {
                    return -1;
                } else return 0;
            } else if (SortMethod.PRICE == method) {
                if (p1.getPrice() > p2.getPrice()) return 1;
                else if (p1.getPrice() < p2.getPrice()) return -1;
                else return 0;
            } else {
                return p1.getTitle().compareTo(p2.getTitle());
            }
        });
    }
}
