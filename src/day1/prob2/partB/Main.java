package day1.prob2.partB;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args){
        List<Product> productList = new ArrayList<Product>();

        productList.add(new Product("IPhone",999.0,2018));
        productList.add(new Product("OnePlus 5T",580.0,2017));
        productList.add(new Product("Galaxy S9",1100.0,2018));
        productList.add(new Product("Google Pixel 2",1150.0,2017));
        productList.add(new Product("Xiaomi Mi Mix", 890.0, 2016));

        Collections.sort(productList,new PriceComparator());
        System.out.println("======== PriceComparator ==========");
        for(Product p:productList){
            System.out.println(p);
        }

        Collections.sort(productList,new TitleComparator());
        System.out.println("======== TitleComparator ==========");
        for(Product p:productList){
            System.out.println(p);
        }
    }
}
